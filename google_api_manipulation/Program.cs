﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Services;

DriveService CreateService()
{
    using (var stream =
        new FileStream("credentials-api.json", FileMode.Open, FileAccess.Read))
    {
        var credentials = GoogleCredential.FromStream(stream);
        if (credentials.IsCreateScopedRequired)
        {
            credentials = credentials.CreateScoped(new string[] { DriveService.Scope.Drive });
        }

        return new DriveService(new BaseClientService.Initializer()
        {
            HttpClientInitializer = credentials,
            ApplicationName = "application name",
        });
    }
}

IList<Google.Apis.Drive.v3.Data.File>? ListFiles(DriveService service)
{
    var request = service.Files.List();
    request.PageSize = 100;
    request.Fields = "nextPageToken, files(id, name)";
    var files = request.Execute().Files;

    if(files == null || files.Count < 1) { Console.WriteLine("No files"); return null; }

    foreach(var file in files)
        Console.WriteLine(file.Id);

    return request.Execute().Files; 
}

void DeleteAll(DriveService service, IList<Google.Apis.Drive.v3.Data.File>? files)
{
    if(files == null || files.Count < 1) { Console.WriteLine("No files"); return; }

    foreach(var file in files)
    {
        var req2 = service.Files.Delete(file.Id);
        req2.Execute();
    }    
    service.Files.EmptyTrash();    
}

var service = CreateService();

var files = ListFiles(service);
DeleteAll(service, files);
ListFiles(service);

