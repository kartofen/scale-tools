#!/bin/sh

if [ "$1" == "" ]; then
    echo "ERROR: provide secrets directory path"
    exit
fi

# finds all files, except anything in .git directory
find $1 -path $1/.git -prune -o -type f -print | while read line; do
    dest="$line"
    dest=${dest#*/}; dest=${dest#*/}; dest=${dest#*/}
    dest="../../$dest"
    source="$line"

    if [ "$2" == "clean" ]; then
        rm $dest
        echo "File removed at '$dest'"
    elif [ "$2" == "list" ]; then
        echo "Source: '$source'"
        echo "Destination: '$dest'"
    else
        cp -r $source $dest
        echo "File at '$source' copied to '$dest'"
    fi

    # ln -s $source $dest
    # echo "File at '$source' linked to '$dest'"
done
